<?php
/**
 * @file
 * sacto_formatters.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function sacto_formatters_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'boolean_yes_no';
  $formatter->label = 'Boolean yes/no';
  $formatter->description = 'Formats the 1 or zero to a yes or no answer.';
  $formatter->mode = 'php';
  $formatter->field_types = 'list_boolean';
  $formatter->code = 'if ($variables["#items"][0]["value"] == 1) {
  print "Yes";
}
else {
  print "No";
}';
  $formatter->fapi = '';
  $export['boolean_yes_no'] = $formatter;

  return $export;
}
