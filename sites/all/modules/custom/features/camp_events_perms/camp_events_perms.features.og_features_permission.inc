<?php
/**
 * @file
 * camp_events_perms.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function camp_events_perms_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:event:add user'
  $permissions['node:event:add user'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:administer group'
  $permissions['node:event:administer group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:approve and deny subscription'
  $permissions['node:event:approve and deny subscription'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:create room content'
  $permissions['node:event:create room content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:create schedule_item content'
  $permissions['node:event:create schedule_item content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:create session content'
  $permissions['node:event:create session content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:create time_slot content'
  $permissions['node:event:create time_slot content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete any room content'
  $permissions['node:event:delete any room content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete any schedule_item content'
  $permissions['node:event:delete any schedule_item content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete any session content'
  $permissions['node:event:delete any session content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete any time_slot content'
  $permissions['node:event:delete any time_slot content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete own room content'
  $permissions['node:event:delete own room content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete own schedule_item content'
  $permissions['node:event:delete own schedule_item content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete own session content'
  $permissions['node:event:delete own session content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:delete own time_slot content'
  $permissions['node:event:delete own time_slot content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:manage members'
  $permissions['node:event:manage members'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:manage permissions'
  $permissions['node:event:manage permissions'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:manage roles'
  $permissions['node:event:manage roles'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:subscribe'
  $permissions['node:event:subscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:event:subscribe without approval'
  $permissions['node:event:subscribe without approval'] = array(
    'roles' => array(
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:unsubscribe'
  $permissions['node:event:unsubscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:event:update any room content'
  $permissions['node:event:update any room content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update any schedule_item content'
  $permissions['node:event:update any schedule_item content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update any session content'
  $permissions['node:event:update any session content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update any time_slot content'
  $permissions['node:event:update any time_slot content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update body field'
  $permissions['node:event:update body field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_accepted field'
  $permissions['node:event:update field_accepted field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_dates field'
  $permissions['node:event:update field_dates field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_event_where field'
  $permissions['node:event:update field_event_where field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_experience field'
  $permissions['node:event:update field_experience field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_image_cache field'
  $permissions['node:event:update field_image_cache field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_registration field'
  $permissions['node:event:update field_registration field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_room_capacity field'
  $permissions['node:event:update field_room_capacity field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_room_photo field'
  $permissions['node:event:update field_room_photo field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_room_slots_types_allowed field'
  $permissions['node:event:update field_room_slots_types_allowed field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_session_timeslot field'
  $permissions['node:event:update field_session_timeslot field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_slides field'
  $permissions['node:event:update field_slides field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_slot_datetime field'
  $permissions['node:event:update field_slot_datetime field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_speakers field'
  $permissions['node:event:update field_speakers field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update field_track field'
  $permissions['node:event:update field_track field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:event:update group'
  $permissions['node:event:update group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update group_group field'
  $permissions['node:event:update group_group field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update og_group_ref field'
  $permissions['node:event:update og_group_ref field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update og_user_node field'
  $permissions['node:event:update og_user_node field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update own room content'
  $permissions['node:event:update own room content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update own schedule_item content'
  $permissions['node:event:update own schedule_item content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update own session content'
  $permissions['node:event:update own session content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:update own time_slot content'
  $permissions['node:event:update own time_slot content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:view body field'
  $permissions['node:event:view body field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_accepted field'
  $permissions['node:event:view field_accepted field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_dates field'
  $permissions['node:event:view field_dates field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_event_where field'
  $permissions['node:event:view field_event_where field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_experience field'
  $permissions['node:event:view field_experience field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_image_cache field'
  $permissions['node:event:view field_image_cache field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_registration field'
  $permissions['node:event:view field_registration field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_room_capacity field'
  $permissions['node:event:view field_room_capacity field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_room_photo field'
  $permissions['node:event:view field_room_photo field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_room_slots_types_allowed field'
  $permissions['node:event:view field_room_slots_types_allowed field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_session_timeslot field'
  $permissions['node:event:view field_session_timeslot field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_slides field'
  $permissions['node:event:view field_slides field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_slot_datetime field'
  $permissions['node:event:view field_slot_datetime field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_speakers field'
  $permissions['node:event:view field_speakers field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_track field'
  $permissions['node:event:view field_track field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:event:view group_group field'
  $permissions['node:event:view group_group field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view og_group_ref field'
  $permissions['node:event:view og_group_ref field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view og_user_node field'
  $permissions['node:event:view og_user_node field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  return $permissions;
}
