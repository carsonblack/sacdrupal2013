<?php
/**
 * @file
 * sacto_reg_extras.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sacto_reg_extras_views_api() {
  return array("version" => "3.0");
}
