<?php
/**
 * @file
 * drupalcamp_sacto_spam_protection.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drupalcamp_sacto_spam_protection_user_default_permissions() {
  $permissions = array();

  // Exported permission: access mollom statistics.
  $permissions['access mollom statistics'] = array(
    'name' => 'access mollom statistics',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'mollom',
  );

  // Exported permission: administer honeypot.
  $permissions['administer honeypot'] = array(
    'name' => 'administer honeypot',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'honeypot',
  );

  // Exported permission: administer mollom.
  $permissions['administer mollom'] = array(
    'name' => 'administer mollom',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'mollom',
  );

  // Exported permission: bypass honeypot protection.
  $permissions['bypass honeypot protection'] = array(
    'name' => 'bypass honeypot protection',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'honeypot',
  );

  // Exported permission: bypass mollom protection.
  $permissions['bypass mollom protection'] = array(
    'name' => 'bypass mollom protection',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'mollom',
  );

  return $permissions;
}
