<?php
/**
 * @file
 * photo_album.features.inc
 */

/**
 * Implements hook_views_api().
 */
function photo_album_views_api() {
  return array("version" => "3.0");
}
