<?php
/**
 * @file
 * sacto_redirect_to_registration.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function sacto_redirect_to_registration_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'default_group_subscribe_groups';
  $strongarm->value = array(
    0 => '1',
  );
  $export['default_group_subscribe_groups'] = $strongarm;

  return $export;
}
