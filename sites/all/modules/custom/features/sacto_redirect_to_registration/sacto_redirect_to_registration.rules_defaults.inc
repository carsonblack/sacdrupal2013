<?php
/**
 * @file
 * sacto_redirect_to_registration.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function sacto_redirect_to_registration_default_rules_configuration() {
  $items = array();
  $items['rules_mark_registration_complete'] = entity_import('rules_config', '{ "rules_mark_registration_complete" : {
      "LABEL" : "Mark registration complete",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "flag", "registration" ],
      "ON" : [ "registration_insert" ],
      "DO" : [
        { "flag_flaguser" : {
            "flag" : "registration_complete",
            "user" : [ "site:current-user" ],
            "flagging_user" : [ "site:current-user" ],
            "permission_check" : 1
          }
        }
      ]
    }
  }');
  $items['rules_redirect_user_to_registration'] = entity_import('rules_config', '{ "rules_redirect_user_to_registration" : {
      "LABEL" : "Redirect user to registration",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : [ "user_login" ],
      "IF" : [
        { "NOT flag_flagged_user" : {
            "flag" : "registration_complete",
            "user" : [ "site:current-user" ],
            "flagging_user" : [ "site:current-user" ]
          }
        }
      ],
      "DO" : [
        { "redirect" : { "url" : "event\\/drupalcamp-sacramento-2013\\/register" } }
      ]
    }
  }');
  return $items;
}
