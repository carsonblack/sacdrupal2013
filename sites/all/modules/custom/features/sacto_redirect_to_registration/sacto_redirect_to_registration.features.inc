<?php
/**
 * @file
 * sacto_redirect_to_registration.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sacto_redirect_to_registration_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function sacto_redirect_to_registration_flag_default_flags() {
  $flags = array();
  // Exported flag: "Registration completed".
  $flags['registration_complete'] = array(
    'content_type' => 'user',
    'title' => 'Registration completed',
    'global' => '0',
    'types' => array(),
    'flag_short' => 'Registration complete',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Registration incomplete',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '3',
        1 => '6',
      ),
      'unflag' => array(
        0 => '3',
        1 => '6',
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_profile' => 0,
    'access_uid' => 'others',
    'api_version' => 2,
    'module' => 'sacto_redirect_to_registration',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}
