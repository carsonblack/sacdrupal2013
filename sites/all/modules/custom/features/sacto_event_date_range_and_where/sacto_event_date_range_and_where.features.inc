<?php
/**
 * @file
 * sacto_event_date_range_and_where.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sacto_event_date_range_and_where_views_api() {
  return array("version" => "3.0");
}
