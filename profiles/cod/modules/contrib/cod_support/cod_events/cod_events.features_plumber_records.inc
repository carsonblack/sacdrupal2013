<?php
/**
 * @file
 * cod_events.features_plumber_records.inc
 */

/**
 * Implements hook_features_plumber_defaults().
 */
function cod_events_features_plumber_defaults() {
  $export = array();

  $features_plumber_record = new stdClass();
  $features_plumber_record->disabled = FALSE; /* Edit this to true to make a default features_plumber_record disabled initially */
  $features_plumber_record->api_version = 1;
  $features_plumber_record->name = 'cod_events__dependencies__commerce_cart';
  $features_plumber_record->feature = 'cod_events';
  $features_plumber_record->component = 'dependencies';
  $features_plumber_record->component_val = 'commerce_cart';
  $export['cod_events__dependencies__commerce_cart'] = $features_plumber_record;

  return $export;
}
